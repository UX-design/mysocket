var app = require( 'express' ) () ;
var http = require( 'http' ).Server( app ) ;
var io = require( 'socket.io' )( http ) ;

// START ---------------------------------------- send a RAW html string to the output
/*
app.get( '/' , function ( req , res ) {
	res.send( '<h1>Hello world</h1>' ) ;
} ) ;
*/
// END   ---------------------------------------- send a RAW html string to the output

// START ---------------------------------------- send a TEMPLATE html file to the output
app.get( '/' , function ( req , res ) {
	res.sendFile( __dirname + '/html/index.html' ) ;
} ) ;
// END   ---------------------------------------- send a TEMPLATE html file to the output

// START ---------------------------------------- if client is connected
io.on( 'connection' , function ( socket ) {

	console.log( 'a user connected' ) ;

	// START ---------------------------------------- if client is disconnected
	socket.on( 'disconnect' , function () {
		console.log( 'user disconnected' ) ;
	} ) ;
	// END   ---------------------------------------- if client is disconnected

	// START ---------------------------------------- if client send an ACTION
	socket.on( 'action' , function ( action ) {
		console.log( 'server receive action : ' + action ) ;
		io.emit( 'action' , action ) ;
	} ) ;
	// END   ---------------------------------------- if client send an ACTION

} ) ;
// END   ---------------------------------------- if client is connected

// START ---------------------------------------- SERVER START
http.listen( 80 , function () {
	console.log( 'listening on *:80' ) ;
} ) ;
// END   ---------------------------------------- SERVER START